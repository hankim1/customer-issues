# [Tenant: airliquide-pbegyqrc ] FW: SSO was enabled for your team for customer Airliquide (Normal) 

## Issue
- Zendesk: https://defense.zendesk.com/agent/tickets/356327
- Tenant: airliquide-pbegyqrc
- Support Tier: Requester: Lionel Charpenay  / l.charpenay@f5.com​
- Description
  - Since SSO was enabled I’ve got [the log in screen](https://gitlab.com/volterra/support/zendesk/uploads/27f999b1b06658bb6e89bae79e283175/image.png):
    Before the SSO I was able to log in with `l.charpenay@f5.com`.
  - Why MACIF SSO is used instead of the Azure one where I can use my current `l.charpenay@f5.com` email ?

## Misc.
- [Issue: [Tenant: airliquide-pbegyqrc ] FW: SSO was enabled for your team for customer Airliquide (Normal) ](https://gitlab.com/volterra/support/zendesk/-/issues/6367)
