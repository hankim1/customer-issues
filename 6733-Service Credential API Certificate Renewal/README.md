# [Tenant: fnz] [FnZ] - Service Credential API Certificate Renewal (Normal)

## Summary
- Issue: https://gitlab.com/volterra/support/zendesk/-/issues/6733
- Customer FnZ is facing an issue with API Certificate renewal and that has been pointed out as a massive blocker and needs urgent attention/fix. 
- Following the document to renew service credentials via the API (https://docs.cloud.f5.com/docs/reference/api-ref/ves-io-schema-api_credential-customapi-renewservicecredentials), API certificate is not getting renewed.
- Current workaround is to find, delete and recreate the expiring certificates from customer end which is really not a solution that can work at scale.

## Details
- The renewal of service credential of type API certificate is not supported (Expiry TS is encoded in the certificate). we only allow the renewal of type API Tokens. This restriction is already taken care of inside the voltconsole. [ref](https://gitlab.com/volterra/support/zendesk/-/issues/6733#note_1470329718)

- Let's update the Docs/API response as required if there is a gap.

