# [Tenant: elementsuite-nbqpkxex] SSO sign-in password prompt leads to Internal Server Error (Normal) 

## Issue
- Zendesk: https://defense.zendesk.com/agent/tickets/359384
- Tenant: elementsuite-nbqpkxex
- Requester: Branislav Muntag  / b.muntag@f5.com​
- Description
  - AIP user (Tim Palmer, tim.palmer@elementsuite.com) from Element Suite is experiencing issue with F5 Distributed cloud login.
  - When he attempts to login the page takes him to password reset which then results in an Internal Server Error message.
  - The user is using Okta as their SSO provider, and he was able to successfully login in the past. No other users in his team are experiencing this error. The user tried incognito mode but that yielded the same Internal server error. Unfortunately, we do not have an HAR file as the user was not comfortable sharing the entire HAR.
Please see screenshots for the issue.
    
    ![](./img/internal-error-message.png)
    
    ![](./img/sso-login-initial-page.png)

## Discussions

- [Tim](https://gitlab.com/volterra/support/zendesk/-/issues/6567#note_1467417357)
  - If you are asking what I did last week: I didn’t trigger a reset-password workflow.
  - If you are asking about what I did when I originally set up the account: 
    - yes I did reset my password. 
    - Here is the email I sent around that time describing the problems:
      ```
        Hi Jared,
        I think I have set up two different F5 accounts, 
        but I still can’t access the F5 distributed cloud console.

        1. I was sent an email with the subject “You are being invited to a MyF5 account”,
           and the registration URL 
           “https://account.f5.com/myf5/signin/registerhttps://account.f5.com/myf5/signin/register”. 
           I have created an account with this.

        2. I then tried to follow the instructions in this email (which I received previously):
      ```

       ![](./img/you-are-now-tenant-owner.png)

      ```
        However the link in this email took me to a different domain (volterra.io not f5).

        3. I tried logging in with my new F5 account, 
           but got an ‘invalid username or password’ message.

        4. I then tried resetting my password from that page 
           (with the domain “login.ves.volterra.io”). 
           
           I did receive a further “Reset password” email as part of this process, 
           but the password reset page hung when submitting the new password. 
           Eventually the server responded with ‘Internal Server Error’.

        5. I tried using the link from step 2 above, with the new password 
           that I might or might not have saved in step 4, but this didn’t work.

        Separately, 
        the help page here [https://docs.cloud.f5.com/docs/how-to/user-mgmt/sso-okta] is a bit confusing. 
        The instructions in the “Configure OIDC Authentication Application in Okta” 
        section seem unrelated to what I’m trying to do. 

        Once I have access I was intending to follow the instructions in the second half of the page (“Enable SSO using Okta in the Console”). 

        If that is the wrong thing to do then please let me know.

        Thanks

        Tim
      ```

- [Response from Jared to Tom]((https://gitlab.com/volterra/support/zendesk/-/issues/6567#note_1467417357))
  ```
    Hey Tim,
    Thanks for sending that over! In regard to your previous questions, I’ve been able to get an answer for you.

    - The “https://account.f5.com/myf5/signin/registerhttps://account.f5.com/myf5/signin/register” (MyF5 Account) is a general support space for other F5 products and not relevant here, you can ignore that. 
    
    - My apologies for the confusion here, we believe this is an automated invite and are working to correct it. 
    
    - Since you’re the Tennant owner this will only be sent to you, so the rest of your users won’t deal with the confusion.
    
    - The invite you received with the domain “login.ves.volterra.io” is the correct invite for how you will be accessing the platform post migration. 
    
    - Your specific url is: https://elementsuite.console.ves.volterra.io/https://elementsuite.console.ves.volterra.io/. 
    
    - To fix your login issue, please navigate to https://elementsuite.console.ves.volterra.io/https://elementsuite.console.ves.volterra.io/ and click the "Forgot Password" link. 
    
    - If you receive the internal error message again, please let me know and we will reset your access.
    
    - As for the SSO, yes once you’re able to login, you can navigate to the "Administration" tile, "Login Options" under "Tenant Settings" in the menu on the left of the window, and click on "Set up SSO" to complete the configuration according to the second half of the instructions you previously referred to.
    
    Best,
    
    Jared
  ```

 - Can someone from eywa team confirm if this tenant "elementsuit" is Volterra managed.

   You could find the volterra managed users (who is a tenant owner who can also login via SSO) as the following table. The tenant owners can also login via SSO since the SSO has been enabled:

   | Identity Management Type | Tenant Owner | Users                   |
   |--------------------------|--------------|-------------------------|
   | `VOLTERRA_MANAGED`       | Yes          | Tim, Jarod(F5), Joe(F5) |
   | `SSO`                    |  No          | Alex, Sam, Will         |


