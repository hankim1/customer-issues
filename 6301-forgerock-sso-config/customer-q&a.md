Customer is getting issues with SSO config with ForgeRock as follow:

- They are integrating **ForgeRock IDP** to **F5 tenant (vzapipoc)** for SSO, We have configured **client id**, **client secret** , **Scope (openId naasmc)**, **auth endpoint**, **token endpoint** using Custom- SSO screens in F5 that take us to the **SSO login screen** and post authenticate it  open the **"Update Account Information"** page and pre populate **email id** in **user id and email**, but **First name and last name** shows **empty**, however SSO is sending the the details as **FirstName , LastName (exact field name)**.

- They wanted to check **why these are not pulled from id_toke** and **pre populated**. 

- Also they are sending another field **naasmc_roles** in the **header** that was also **missing** when we validated the **response header**.

- They are looking **how they can debug** this issue and from the above provided details if you can guide us.

- We have verify on F5 Docs for SSO `ForgeRock` or any other SSO methods, but there were `no guide** for this, also we have indicated them to check under `Audit Logs & Alerts` - Audit Logs, but they were **not seeing any error** in Audit log even not any logs for the user login via SSO.

- Their question **when the user login via SSO** **userid, firstName, lastName, email should be prefilled** and user should not enter is even very first time as those are already sent in id_token. Wondering if there is **specific keys** to pass those in **id_token**.

- Could you please help us to check this?

- Please let us know if something else is required on this.
