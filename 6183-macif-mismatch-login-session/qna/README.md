
- Q1. Which user information do you receive in JWT Access Token or ID Token in exchange to this oAuth code?
- A1. The XC gets the user information from the ID token. It is set by this rule (email, preferred_username, or unique_name).


- Q2. Do you have in XC logs the relative Access Token and ID Token exchanged for this code?
  > Note:
  > - I want to see if the user name is Alexis or Pawel in the ID Token.
  >   - If it is Pawel, the issue is form customer side.
  >   - If it is Alexis, the issue is form F5 side.
- A2. The XC doesn't log the ID token.


- Q2. Is the error on XC Console side or customer Authorization Server side?
- A2.
  - I would like to double check if the AuthZ server setup is correctly again although you tried using NGINX API GW in the video.
  - Looks like the customer is using PingOne in the video.
  - 



  - I watched the video that you tested via NGINX GW w/ PingOne.
  - To make sure if AuthZ server setup is correct, can you test the config again using this tool?
    - [NGINX OIDC w/ Ping Identity](https://github.com/nginx-openid-connect/nginx-oidc-ping-identity)
    - [Create and configure an app in Ping Identity](https://github.com/nginx-openid-connect/nginx-oidc-ping-identity/blob/main/docs/01-IdP-Setup.md)
    - [Configure NGINX Plus OIDC](https://github.com/nginx-openid-connect/nginx-oidc-ping-identity/blob/main/docs/02-NGINX-Plus-Setup.md)
    - [Locally Test an SSO app in a container](https://github.com/nginx-openid-connect/nginx-oidc-ping-identity/blob/main/docs/03-Container-Test.md)

