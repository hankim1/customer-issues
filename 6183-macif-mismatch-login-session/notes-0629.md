**Customer Meeting Notes (6/28)**

**RESULTS**
- The actions are done this morning based on the [STR (6/27)](https://gitlab.com/volterra/support/zendesk/-/issues/6183#note_1449901781) to resolve the issue for the following accounts 
  - Nicolas QUANTIN <nquantin@macif.fr>
  - Alexis DA COSTA<alexis.dacosta.externe@macif.fr>
  - Pawel Kuligowski - V<pawel.kuligowski.externe@macif.fr>
 
**ISSUES RESOLVED**
- SSO User nquantin@macif.fr is seen logged as alexis.dacosta.externe@macif.fr in XC Console
- SSO User pawel.kuligowski.externe@macif.fr  is seen logged as alexis.dacosta.externe@macif.fr in XC Console
- SSO User alexis.dacosta.externe@macif.fr  is seen logged as pawel.kuligowski.externe@macif.fr  in XC Console
 
**ACTIONS DONE**
- F5 Access: ​[issue_macif_actions_2023_06_28.mp4](https://f5-my.sharepoint.com/:v:/p/al_dacosta/Edn5Z6NuiUpMiN7gsBdK_xEBl0Cbu7Boso0x6k_AfNPNpA)
- Nicolas access: ​[issue_macif_actions_2023_06_28.mp4](https://f5-my.sharepoint.com/:v:/p/al_dacosta/Edn5Z6NuiUpMiN7gsBdK_xEBxxHSn9H28-8-w_9uzsa4Ig)

**NEXT STEPS**
- Nicolas QUANTIN will ask teammates if they currently have an issue with SSO login
- Nicolas QUANTIN create an external MACIF account for Lionel Charpenay l.charpenay@f5.com
- Pawel Kuligowski - V + Lionel Charpenay meeting 6/29 : onboarding of customers Mobile Phones
- Alexis DA COSTA, @hankim1: debrief meeting


**Additional Meeting Notes (6/28)**
- Alexis DA COSTA, @kabi.patt, @hankim1 
- Recommendation To Customer
  - Expose the OIDC endpoint
    - `"ping_end_session_endpoint": "https://fisso-hub.macif.fr/idp/startSLO.ping",`
    - Currently, the logout endpoint is not exposed in the customer system, so the request is rejected, and the session is not cleaned up.
    - So different accounts can be used in the same browser.
  - Configure family name in the customer's Ping Identity


# Notes
- [6/27, debugging](https://gitlab.com/volterra/support/zendesk/-/issues/6183#note_1449901781)
- [6/28, customer meeting notes](https://gitlab.com/volterra/support/zendesk/-/issues/6183#note_1450053950)
