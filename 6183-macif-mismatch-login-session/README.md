# Tenant: macif Mismatch error during login session (High) 

Customer is reporting issues in XC console logging session:

- When I try logging with an **account A using SSO**, I'm successfully **logged in** but with an **account B**.

- Please find attached the [HAR file](./log/log_mismatch_alexis_pawel_macif_xc.har.json) during logging session.

- The Authorization Server redirected me to XC Console using oAuth code (in the HAR the oAuth code is 7fNbrHWkcTgzROodQYcGx8VT0l2_cyG0M_cAAAAC at line 2963.

- Which user information do you receive in JWT Access Token or ID Token in exchange to this oAuth code?


- Is the error on **XC Console side** or **customer Authorization Server side**?
  log_mismatch_alexis_pawel_macif_xc.har.txt

Support: We have asked them for any HAR, image, Video reproducion the issue, Customer/F5er provided and video with the issue:

- video and HAR file are available in this folder:
  https://f5-my.sharepoint.com/:f:/p/al_dacosta/Eqylt0zTDLNJnQh820Ylvy0BFV34vZ-gFCiBh9S99NFIlw?e=1o54Zh

- In HAR file, the oAuth Authorization code is EtmuVSihZ12UTGAVI32PSkio23s3O5L0JgsAAAAC.

- Do you have in XC logs the relative Access Token and ID Token exchanged for this code?
  - I want to see if the user name is Alexis or Pawel in the ID Token.
    - If it is Pawel, the issue is form customer side.
    - If it is Alexis, the issue is form F5 side.


- We have several Zoom meetings to test customer environments:
  | Date | PDT         | CET-France | Participants                                                     | Remark                  |
  |------|-------------|------------|------------------------------------------------------------------|-------------------------|
  | 6/22 | 8am-9:30am  | 5pm-6:30pm | @hankim1 , Alexis DA COSTA(Solution Engineering)                 | `Done` w/ configuration |
  | 6/23 | 8am-9:30am  | 5pm-6:30pm | @hankim1 , Alexis DA COSTA(Solution Engineering), @f5xc-sre-ops  | `TODO` w/ user-alexis   |
  | TBD  | TBD         | TBD        | @hankim1 , Alexis DA COSTA(Solution Engineering), @f5xc-sre-ops  | `TODO` after customer registers additional test users  |
  | TBD  | TBD         | TBD        | @hankim1 , Alexis DA COSTA, @f5xc-sre-ops, Customer              | `TODO` w/ customer      |

- To try finding the root-cause, we need to test several scenarios (X users, options) and check the Keycloak DB and behaviours.
- Could anyone in the SRE team join Zoom meetings and be assigned here to work together as SRE team has the access to Keycloak Admin console? So I can invite the meetings to the assignee in the SRE team (or you could give me access to the Keycloak admin console.) 

- Investigation
  - [6/23 Investigation & Meeting Notes](https://gitlab.com/volterra/support/zendesk/-/issues/6183#note_1444020702)

- MACIF Cellphone Enrollment 

  Guide:

  ![](./img/macif-phone-enrollment-guide.png)
  
  Error:

  ![](./img/macif-phone-enrollment-err.png)


- OIDC Link Error
  - ![](./img/link-oidc-email.png)
  - ![](./img/link-oidc-error.png)
