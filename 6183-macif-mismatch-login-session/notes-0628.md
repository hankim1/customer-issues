**Meeting Notes (6/27)**

- Alexis DA COSTA, @kabi.patt, @s.tadepalli, @hankim1
- **Steps To Reproduce(STR)** for OIDC link procedure issue
  - 1. SSO Setup with this [well-known-endpoints-info.json](/uploads/500fd8274906b7d7d3527e3e97ae066e/well-known-endpoints-info.json)
  - 2. SSO Login using the account of alexis.dacosta.externe@macif.fr
  - 3. Login as a tenant owner to https://airliquide.console.ves.volterra.io
  - 4. Attach a customer user-group (`custom-read-all`) to the alexis.dacosta.externe@macif.fr
  - 5. Open another browser for SSO login using the account of alexis.dacosta.externe@macif.fr (Because, the MACIF doesn't support logout endpoint in their API gateway configuration, and it is rejected now.)
  - 6. Encountered an issue (OIDC linking procedure in an infinite loop) when attaching the customer user-group to the Alexis' account (alexis.dacosta.externe@macif.fr) for the first time for some reason.

      | OIDC Link Email | Keycloak endpoint (`/realms/xx/login-actions/action-token`) error  |
      | ------ | ------ |
      | ![link-oidc-email](/uploads/fdaccd87d4e81b0efef77b5de0d2260c/link-oidc-email.png)       | ![link-oidc-error](/uploads/8e59ec71852d15ccc72aad0005daf3fa/link-oidc-error.png)       |

      ```
      https://login.ves.volterra.io/auth/realms/airliquide-pbegyqrc/login-actions/action-token?key=eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJmMzViMmM0Ny0xMzQxLTRiNmYtOTNmNS1iZGYyNDc2MzMwZmQifQ.eyJleHAiOjE2ODc1Mzg1MDQsImlhdCI6MTY4NzUzNjcwNCwianRpIjoiMzM5Zjk1YTMtY2M4Zi00MzViLWJkZWYtNmU3ODM0NmRlZGZlIiwiaXNzIjoiaHR0cHM6Ly9sb2dpbi52ZXMudm9sdGVycmEuaW8vYXV0aC9yZWFsbXMvYWlybGlxdWlkZS1wYmVneXFyYyIsImF1ZCI6Imh0dHBzOi8vbG9naW4udmVzLnZvbHRlcnJhLmlvL2F1dGgvcmVhbG1zL2FpcmxpcXVpZGUtcGJlZ3lxcmMiLCJzdWIiOiI5NTg4ZjY2Mi1jNWQ1LTQ0YWYtYTE4Zi1kMDk1NTJlMzRjN2EiLCJ0eXAiOiJpZHAtdmVyaWZ5LWFjY291bnQtdmlhLWVtYWlsIiwiYXpwIjoidmVzLW9pZGMtYWlybGlxdWlkZS1wYmVneXFyYyIsIm5vbmNlIjoiMzM5Zjk1YTMtY2M4Zi00MzViLWJkZWYtNmU3ODM0NmRlZGZlIiwiZW1sIjoiYWxleGlzLmRhY29zdGEuZXh0ZXJuZUBtYWNpZi5mciIsImlkcHUiOiJhbGV4aXMuZGFjb3N0YS5leHRlcm5lQG1hY2lmLmZyIiwiaWRwYSI6Im9pZGMiLCJhc2lkIjoiZDRkNWY2ZDAtYzI2Yy00NDU4LThjMjMtYzRkNGY5ZTllNmE2LjA0N0wwVGozVVFJLmNlMzhlMDQ2LWY2MmUtNDc1ZC1iZjE3LTJlMDc2NTg1ZjYzNyIsImFzaWQiOiJkNGQ1ZjZkMC1jMjZjLTQ0NTgtOGMyMy1jNGQ0ZjllOWU2YTYuMDQ3TDBUajNVUUkuY2UzOGUwNDYtZjYyZS00NzVkLWJmMTctMmUwNzY1ODVmNjM3In0.ncCd37t25EULdD6ebN-maf_JdumU7Ly0dSx8ZEnr2Ss&client_id=ves-oidc-airliquide-pbegyqrc&tab_id=047L0Tj3UQI&execution=f4d66ae5-89e6-4577-aee8-99618fec87eb
      ```
  - Found that the `usernames` is empty in the custom-group for the first time as the above step 6.
    ```json
    {
      "id": "481d9420-8dbc-4674-bf35-74549f3965c5",
      "name": "custom-read-all",
      "usernames": [],
    }
    ```
    Details: [01-empty-user-in-custom-read-all-group.json](/uploads/2435be1eeae5ff752f31a564ef911531/01-empty-user-in-custom-read-all-group.json)

- Edit the user an additional group (e.g. `managed-monitor`) to the same user
  - Navigate to `IAM > Users`
  - Select the account
  - `Edit User > Assign User to Group`
  - Select `managed-monitor`, and `Assign to Group`
  - Click `Save Changes`

    ```json
    {
      "id": "6a70b629-daa9-4087-9344-e979ec6b4597",
      "name": "managed-monitor",
      "usernames": [ "alexis.dacosta.externe@macif.fr" ]
    }
    ```
    Details: 
[02-add-group-managed-monitor-to-the-user.json](/uploads/f5c93a1e8908960fb2913b043ad3965c/02-add-group-managed-monitor-to-the-user.json)
- Either edit group or user to attach the `custom-read-all` to the same user again.
  ```json
  {
    "id": "481d9420-8dbc-4674-bf35-74549f3965c5",
    "name": "custom-read-all",
    "usernames": [ "alexis.dacosta.externe@macif.fr" ]
  }
  ```    
- OIDC link error was resolved. (But we need more test to see why the usernames were empty for some reason between XC and customer's configuration)

