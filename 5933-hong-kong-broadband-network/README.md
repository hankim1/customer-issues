# [Tenant: ] Request for decommission of Hong Kong Broadband Network Limited - Tenant: hkbn-itsa-infra-thuhsgre (Normal) 

## Request

- Please can we start the process to decommission the below tenant. Customer has been emailed and made aware.
  - Opportunity Name: HKBN/ Shape_IBD/ HKBN/ ACW
  - SF Link:
    - https://f5.lightning.force.com/lightning/r/Opportunity/0061T00000vol01QAA/view
    - Opportunity Owner: Boris Mok - b.mok@f5.com
    - Account Name: Hong Kong Broadband Network Limited
    - Account Billing Country: Hong Kong
    - Volterra Specialist: N/A
    - Type: Distributed Cloud - Volterra
    - Tenant Name: hkbn-itsa-infra-thuhsgre
    - Tenant Owner: alan.ko@hkbn.com.hk
    - Customer Address: N/A

- Please **decommission this tenant**  , the **customer did not renew** their subscription for this tenant and is not using the service within it – **email confirmation attached**.
- As an FYI – there is another thread out there for this customer that is related to **DDoS managed** and is a different opportunity and the customer confirmed they will treat it separately, so it will not effect this request


## Progress

- Shang Li

  - https://gitlab.com/volterra/support/zendesk/-/issues/5900
  - [4. Run tenant removal command](https://gitlab.com/volterra/ves.io/sre-deployment/-/wikis/Tenant%20operation%20guide#4-run-tenant-removal-command)

    Login to the shell of eywa on gc01-cle.

    ```bash
    kubectl exec -it -n ves-system deployment/eywa -c eywa -- /bin/bash
    ```

    Run following command

    ```bash
     eywad tenant cascade-delete --name=enterpise_tenant_name
    ```

    ```bash
    eywad tenant cascade-delete --uid=enterpise_tenant_uid
    ```

  - [remove ongoing for hkbn-itsa-infra-thuhsgre](https://gitlab.com/volterra/ves.io/sre-prod-model/-/commit/cb7ccccd8d076ff8a75825af12829395be026ecb)

    - https://gitlab.com/volterra/ves.io/sre-prod-model/-/blob/bff24a599b0bdff5fc25aa9846644ede29f2c926/ongoing/sed/objects/customer/hkbn-itsa-infra-thuhsgre/bot-defense-subscription.yml

      ```yaml
        commands:
        - data:
            '@type': ves.io.sed.subscription.Object
            metadata:
            name: bot-defense-subscription
            namespace: system
            labels: {}
            annotations: {}
            description: ""
            disable: false
            uid: b250707e-8667-4a3b-85be-fe28f45ec6d8
            system_metadata:
            uid: b250707e-8667-4a3b-85be-fe28f45ec6d8
            tenant: hkbn-itsa-infra-thuhsgre
            spec:
            gc_spec:
                suspended: false
      ```
