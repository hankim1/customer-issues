# Tenant Decommissioning

## Disable Tenant

**`POST /ves.io.eywa/introspect/write/private/custom/tenant/delete`**

- Request Body:
  ```json
    {
        "email": "h.kim3@f5.com",
        "feedback": "test-deletion",
        "name": "saas-2302-03-xnopjvyq",
        "reason": "test"
    }  
  ```

- HTTP Headers:
    ```json
    {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Access-Control-Allow-Methods": "*",
        "Access-Control-Allow-Origin": "*"
    }
    ```

- Response: Custom API Error
    ```json
    {
        "headers": {
            "normalizedNames": {},
            "lazyUpdate": null
        },
        "status": 400,
        "statusText": "OK",
        "url": "https://compass-lma.demo1.volterra.us/introspection/gc01.int.ves.io/eywa/ves.io.eywa/introspect/write/private/custom/tenant/delete",
        "ok": false,
        "name": "HttpErrorResponse",
        "message": "Http failure response for https://compass-lma.demo1.volterra.us/introspection/gc01.int.ves.io/eywa/ves.io.eywa/introspect/write/private/custom/tenant/delete: 400 OK",
        "error": {
            "code": 3,
            "details": [],
            "message": "unknown value \"test\" for enum ves.io.eywa.uam.tenant.DeletionReason"
        }
    }
    ```

**`POST /ves.io.eywa/introspect/write/private/custom/tenant/delete`**

- Request Body:
  ```json
    {
        "name": "saas-2302-03-xnopjvyq"
    }  
  ```

- HTTP Headers:
    ```json
    {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Access-Control-Allow-Methods": "*",
        "Access-Control-Allow-Origin": "*"
    }
    ```

- Response: Custom API Error
    ```json
    {
        "headers": {
            "normalizedNames": {},
            "lazyUpdate": null
        },
        "status": 500,
        "statusText": "OK",
        "url": "https://compass-lma.demo1.volterra.us/introspection/gc01.int.ves.io/eywa/ves.io.eywa/introspect/write/private/custom/tenant/delete",
        "ok": false,
        "name": "HttpErrorResponse",
        "message": "Http failure response for https://compass-lma.demo1.volterra.us/introspection/gc01.int.ves.io/eywa/ves.io.eywa/introspect/write/private/custom/tenant/delete: 500 OK",
        "error": {
            "code": 13,
            "details": [
                {
                    "type_url": "type.googleapis.com/ves.io.stdlib.server.error.Error",
                    "value": "aerror during validating tenant permission: user not found for tenant: ves-io, user: h.kim3@ves.io:2023-06-13 19:47:57.912296563 +0000 UTC m=+41672.355183395"
                }
            ],
            "message": "error during validating tenant permission: user not found for tenant: ves-io, user: h.kim3@ves.io"
        }
    }
    ```
