

Regarding the issue of [Hong Kong Broadband Network](https://gitlab.com/volterra/support/zendesk/-/issues/5933),

- step 1. Deleted tenants in XC Console -> I can't login the tenant any more as expected.
  - #1 test. `saas-2302-02-ebookvfb`
  - #2 test. `saas-2302-03-xnopjvyq`
- step 2. Both of tenants were successfully removed in ves.io.eywa.uam.tenant.Object
- step 3. Run tenant removal command
  ```bash
  kubectl exec -it -n ves-system deployment/eywa -c eywa -- /bin/bash
  eywad tenant cascade-delete --name=saas-2302-02-ebookvfb
  eywad tenant cascade-delete --name=saas-2302-03-xnopjvyq
  ```
- step 4. Error message: Keycloak (realm not found): 
  - Assume that the realm is removed from the Keycloak in the above step 1.
  - Although this result shows error, the tenant is successfully decommissioned.

```bash
bash-4.4# eywad tenant cascade-delete --name=saas-2302-02-ebookvfb
tenant-cascade-delete command
{"severity":"INFO","time":"2023-06-13T20:39:02.797Z","message":"Running without embedded etcd","messageid":"be2088b9-66c1-4348-b93e-17705d6201cb"}
{"severity":"INFO","time":"2023-06-13T20:39:02.825Z","message":"Info log","trace_info":"","manager":"common","info":"message:reloading system configuration from db now","messageid":"1b97f47b-9e8b-4860-bbf9-964736ff4ed3"}
2023/06/13 20:39:02 Setting up usage client manager for endpoint akar
2023/06/13 20:39:02 Setting up usage client manager for endpoint maurice
2023/06/13 20:39:02 Setting up usage client manager for endpoint blindfold
2023/06/13 20:39:02 Setting up usage client manager for endpoint tpmauthority
2023/06/13 20:39:02 Setting up usage client manager for endpoint client-side-defense
2023/06/13 20:39:02 Setting up usage client manager for endpoint josef
2023/06/13 20:39:02 Setting up usage client manager for endpoint sed
2023/06/13 20:39:02 Setting up usage client manager for endpoint bifrost
2023/06/13 20:39:02 Setting up usage client manager for endpoint viaconnector
2023/06/13 20:39:02 Setting up usage client manager for endpoint minerva
2023/06/13 20:39:02 Setting up usage client manager for endpoint keypr
2023/06/13 20:39:02 Setting up usage client manager for endpoint bolt
2023/06/13 20:39:02 Setting up usage client manager for endpoint asterix
2023/06/13 20:39:02 Setting up usage client manager for endpoint athena
{"severity":"INFO","time":"2023-06-13T20:39:02.892Z","message":"Info log","message":"reloading uam configuration from db now","messageid":"1b97f47b-9e8b-4860-bbf9-964736ff4ed3"}
{"severity":"INFO","time":"2023-06-13T20:39:04.556Z","message":"Info log","trace_info":"","manager":"uam","info":"successfully deleted all namespaces in tenant: saas-2302-02-ebookvfb","messageid":"1b97f47b-9e8b-4860-bbf9-964736ff4ed3"}
{"severity":"INFO","time":"2023-06-13T20:39:04.697Z","message":"Info log","trace_info":"","manager":"uam","info":"finished processing api credential objects in tenant: saas-2302-02-ebookvfb err: 0 errors occurred:\n\t\n\n","messageid":"1b97f47b-9e8b-4860-bbf9-964736ff4ed3"}
{"severity":"INFO","time":"2023-06-13T20:39:04.724Z","message":"Info log","trace_info":"","manager":"uam","info":"deleted tenant configuration in tenant: saas-2302-02-ebookvfb err: <nil>","messageid":"1b97f47b-9e8b-4860-bbf9-964736ff4ed3"}
{"severity":"INFO","time":"2023-06-13T20:39:04.897Z","message":"Info log","trace_info":"","manager":"uam","info":"finished processing all policy objects in tenant: saas-2302-02-ebookvfb err: 0 errors occurred:\n\t\n\n","messageid":"1b97f47b-9e8b-4860-bbf9-964736ff4ed3"}
{"severity":"INFO","time":"2023-06-13T20:39:04.981Z","message":"Info log","trace_info":"","manager":"uam","info":"finished processing oid provider objects in tenant: saas-2302-02-ebookvfb err: 0 errors occurred:\n\t\n\n","messageid":"1b97f47b-9e8b-4860-bbf9-964736ff4ed3"}
2023/06/13 20:39:05 Error: Request returned a response with status '404 Not Found' and body: {"error":"Realm not found."}
2023/06/13 20:39:05 error deleting realm from keycloak err:  404 Not Found
{"severity":"INFO","time":"2023-06-13T20:39:15.867Z","message":"Info log","trace_info":"","manager":"uam","info":"finished processing quota objects in tenant: saas-2302-02-ebookvfb err: 0 errors occurred:\n\t\n\n","messageid":"1b97f47b-9e8b-4860-bbf9-964736ff4ed3"}
Error: 1 error occurred:
	* error in cleanup realm objects: error deleting realm: saas-2302-02-ebookvfb for tenant: 404 Not Found


Usage:
  eywad tenant cascade-delete [flags]

Examples:
eywad tenant cascade-delete --name=tenant-name

Flags:
      --cname string   Tenant which must be deleted by cname
      --disable        If specified means disable tenant before deletion
  -h, --help           help for cascade-delete
      --name string    Tenant which must be deleted by name
      --uid string     Tenant which must be deleted by uid

Global Flags:
      --config string   config file (default "/etc/ves/eywa.yml")

1 error occurred:
	* error in cleanup realm objects: error deleting realm: saas-2302-02-ebookvfb for tenant: 404 Not Found
```

I think because of this error, all the objects for the tenant may not have been deleted, could you confirm if the oidc_broker / quota / usage objects have also been deleted for these tenants? (edited) 

- deleted_tenant: `saas-2302-02-ebookvfb`, `saas-2302-03-xnopjvyq`
- oidc_broker: not deleted but `disable: true`
- quota: can't find it
- usage: can't find it

As I understand, the error is not expected since the realm should exist in kc and be cleaned up as part of this command, is this error consistent across tenants? (edited) 

- The error seems consistent across tenants.
- But, I am trying it with more tenants

did we confirm in kc that the realm indeed does not exist? (edited) 

- kcdmux_realm object: not deleted but `disable: true`
- KC inside: Trying getting access


