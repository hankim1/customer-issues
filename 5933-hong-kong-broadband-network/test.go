package main

import (
	"fmt"

	"github.com/Nerzal/gocloak"
)

// eywad iam set-oidc-client-kc-id --dry-run=false --tenant-id=original-eicigfhi
// eywad iam set-oidc-client-kc-id --dry-run=false --tenant-id=singhania-erfjheia

func mainKcClient() {
   gc := gocloak.NewClient("http://localhost:9445")
   cname := "saas-0616-02"
   tenant := cname + "-ujoqmyjb"
   jwt, err := gc.LoginAdmin("eywa-admin", "SQU1Ca1hObjA3", "master")
   fmt.Printf("jwt '%+v' err %+v \n", jwt.AccessToken, err)

   /*err = gc.CreateRealm(jwt.AccessToken, gocloak.RealmRepresentation{
      ID:                                  tenant,
      Realm:                               tenant,
      DisplayName:                         tenant,
      LoginTheme:                          "volterra",
      EmailTheme:                          "volterra",
      Enabled:                             true,
      SslRequired:                         "all",
      ResetPasswordAllowed:                true,
      ActionTokenGeneratedByAdminLifespan: 86400,
      ActionTokenGeneratedByUserLifespan:  1800,
      SsoSessionIdleTimeout:               1800,
      AccessCodeLifespanUserAction:        900,
      AccessCodeLifespanLogin:             1800,
      RegistrationAllowed:                 false,
      LoginWithEmailAllowed:               true,
      DuplicateEmailsAllowed:              false,
      EditUsernameAllowed:                 false,
      BrowserSecurityHeaders: map[string]string{
         "contentSecurityPolicy":           "frame-src 'self' *.trustarc.com *.demdex.net; frame-ancestors 'self'; object-src 'none';",
         "contentSecurityPolicyReportOnly": "",
         "strictTransportSecurity":         "max-age=31536000; includeSubDomains",
         "xContentTypeOptions":             "nosniff",
         "xFrameOptions":                   "SAMEORIGIN",
         "xRobotsTag":                      "none",
         "xXSSProtection":                  "1; mode=block",
      },
      BruteForceProtected:          true,
      PermanentLockout:             false,
      MaxFailureWaitSeconds:        900,
      MinimumQuickLoginWaitSeconds: 60,
      WaitIncrementSeconds:         60,
      QuickLoginCheckMilliSeconds:  3000,
      MaxDeltaTimeSeconds:          43200,
      FailureFactor:                6,
      EventsEnabled:                true,
      EventsExpiration:             7862400,
      AdminEventsEnabled:           true,
      AdminEventsDetailsEnabled:    true,
      Attributes: map[string]string{
         "actionTokenGeneratedByUserLifespan.reset-credentials": "86400",
      },
      PasswordPolicy: "length(7) and notUsername(undefined) and forceExpiredPasswordChange(90) and hashAlgorithm(pbkdf2-sha256) and hashIterations(10000) and passwordHistory(4) and specialChars(1) and digits(1) and upperCase(1) and lowerCase(1)",
      EventsListeners: []string{
         "jboss-logging",
         "email",
         "metrics-listener",
         "login_event_listener",
      },
      RequiredActions: []interface{}{},
      SMTPServer: map[string]string{
         "port":            "587",
         "host":            "email-smtp.us-west-2.amazonaws.com",
         "from":            "no-reply@cloud.f5.com",
         "fromDisplayName": "F5 Distributed Cloud User Management",
         "auth":            "true",
         "ssl":             "false",
         "starttls":        "true",
         "user":            "AKIAWKKJE7RJDCVDRFWV",
         "password":        "BIJb3vhXTgM2ucRlSQVDQveSkoMfczCM/yTVmkDLnlHD",
         "replyTo":         "no-reply@cloud.f5.com",
         "envelopeFrom":    "no-reply@cloud.f5.com",
      },
      Clients: []gocloak.Client{
         {
            ID:                      "ves-oidc-" + tenant,
            ClientID:                "ves-oidc-" + tenant,
            Name:                    tenant,
            BaseURL:                 fmt.Sprintf("https://%s.demo1.volterra.us/", cname),
            SurrogateAuthRequired:   false,
            Enabled:                 true,
            ClientAuthenticatorType: "client-secret",
            Secret:                  "c07f0837-ab9c-4158-a28c-7a906221eb19",
            RedirectURIs: []string{
               fmt.Sprintf("https://%s.demo1.volterra.us/", cname),
               fmt.Sprintf("https://%s.demo1.volterra.us/web/*", cname),
            },
            Protocol: "openid-connect",
         },
      },
   })
   fmt.Printf("err: %+v\n", err)*/

   realm, err := gc.GetRealm(jwt.AccessToken, tenant)
   fmt.Printf("realm: %+v\n", realm)
   fmt.Printf("err: %+v\n", err)

   clients, err := gc.GetClients(jwt.AccessToken, tenant, gocloak.GetClientsParams{
      //ClientID:     "ves-oidc-" + tenant,
      ViewableOnly: false,
   })
   fmt.Printf("clients: %+v\n", clients)
   fmt.Printf("err: %+v\n", err)
   /*for _, client := range *clients {
   fmt.Printf("client name: '%+v' '%+v'\n", client.ClientID, client.Name)
   /*newClient := gocloak.Client{
      ID:                      "test-abc",
      ClientID:                "test-abc",
      Name:                    "test-name",
      BaseURL:                 "https://" + cname + ".demo1.volterra.us/",
      SurrogateAuthRequired:   false,
      Enabled:                 true,
      ClientAuthenticatorType: "client-secret",
      Secret:                  "client-secret",
      RedirectURIs: []string{
         "https://" + cname + ".demo1.volterra.us/",
         "https://" + cname + ".demo1.volterra.us/web/*",
         "https://" + cname + ".demo1.volterra.us/managed_tenants/*",
      },
      Protocol: "openid-connect",
   }
   err = gc.CreateClient(jwt.AccessToken, tenant, newClient)*/
   /*client.RedirectURIs = []string{
      "https://" + cname + ".demo1.volterra.us/",
      "https://" + cname + ".demo1.volterra.us/web/*",
      "https://" + cname + ".demo1.volterra.us/managed_tenants/*",
   }
   client.Secret = "client"
   err = gc.UpdateClient(jwt.AccessToken, tenant, client)
   fmt.Printf("err: %+v\n", err)*/
   //fmt.Printf("err: '%+v' creating client id '%s' client id '%s'\n", err, newClient.ID, newClient.ClientID)
   //}
}